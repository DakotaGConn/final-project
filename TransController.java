import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


/**
 * Created by Tim McGowen on 3/22/2017
 *
 * This is the controller for the FXML document that contains the view. 
 */
public class TransController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtInTrans;
  
  @FXML
  private TextField txtOutTrans;
  
  @FXML
  private Label lblOutTrans;
  
  @FXML
  private ComboBox outLang;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    TransModel translator = new TransModel();
    
    final ComboBox outLang = new ComboBox();
    outLang.getItems().addAll(translator.langInput());




    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
      String input = txtInTrans.getText();
      if (translator.isValid() == true)
      {
        lblOutTrans.setText(translator.getTrans(input));
      }
      
      else
      {
        lblOutTrans.setText("ERROR: The current input cannot be translated. Please choose a different input.");
      }
    }
  }
  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
}
}
