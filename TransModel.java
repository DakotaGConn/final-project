import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import java.lang.Character;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;
import javafx.scene.control.ChoiceBox;

/**
 * Created by Tim McGowen on 3/22/2017
 *
 * Model to get weather information based on zipcode.
 */
public class TransModel {
  private JsonElement jse1;
  private JsonElement jse2;
  private JsonElement jse3;
  private JsonElement jse4;
  private final String apiKey = "AIzaSyC8LgxT1AhnGl5sMGhKrrjfVU8QBFozS4o";
  private String input;
  private String inTrans;
  private String inLang;
  private String outLang;

  public JsonArray langInput()
  {
    
    try
    {
      URL transURL = new URL("https://translation.googleapis.com/language/translate/v2/languages?key=" + apiKey);

      // Open connection
      InputStream is = transURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse1 = new JsonParser().parse(br);

      // Close connection
      is.close();
      br.close();
    }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }

    JsonArray langs = jse1.getAsJsonObject().get("data").getAsJsonObject()
      .get("languages").getAsJsonArray();
    
    // Check to see if the zip code was valid.
    return langs;
  }

  
  
  public String outLang(int langInput)
  { 
   
    // Check to see if the zip code was valid.
    return langInput().getAsJsonObject().get("language").getAsString();
  }
  
  public String returnOutLang(String outLang)
  {
    
    return outLang;
  }
  
  public String getTrans(String inTrans)
  {  
    try
    {
      URL transURL = new URL("https://translation.googleapis.com/language/translate/v2?target=" + returnOutLang("") + "&q=" + inTrans + "&key=" + apiKey);

      // Open connection
      InputStream is = transURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse4 = new JsonParser().parse(br);

      // Close connection
      is.close();
      br.close();
    }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }

    // Check to see if the zip code was valid.
    return jse4.getAsJsonObject().get("data").getAsJsonObject()
    .get("translations").getAsJsonArray().get(0).getAsJsonObject().get("translatedText").getAsString();
  }

  public String inLang(int langInput)
  { 
   
    // Check to see if the zip code was valid.
    return langInput().getAsJsonObject().get("language").getAsString();
  }
  
  public boolean isValid()
  {
    if ((inTrans.equals(getTrans(""))) && (!(inLang.equals(outLang))))
    {
      return false;
    }
    
    else
    {
      return true;
    }
  }

 }
