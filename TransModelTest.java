import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;


public class TransModelTest {

   @Test
   public void testGetTrans1()
   {
      TransModel translator = new TransModel();
      String Lang = translator.returnOutLang("es");
      String input = translator.getTrans("Hello");
      assertEquals("Hola", input);
      assertEquals(true, translator.isValid());
   }
}
